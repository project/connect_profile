core = 8.x
api = 2

; Modules

projects[blazy][subdir] = "contrib"
projects[blazy][version] = "2.4"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.8"

projects[slick][subdir] = "contrib"
projects[slick][version] = "2.3"

projects[slick_views][subdir] = "contrib"
projects[slick_views][version] = "2.4"

projects[token][subdir] = "contrib"
projects[token][version] = "1.9"

projects[admin_toolbar][subdir] = "contrib"
projects[admin_toolbar][version] = "2.4"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "3.7"

projects[config_update][subdir] = "contrib"
projects[config_update][version] = "1.7"

projects[block_field][subdir] = "contrib"
projects[block_field][version] = "1.0-rc2"

projects[simple_megamenu][subdir] = "contrib"
projects[simple_megamenu][version] = "1.1"

projects[social_media_links][subdir] = "contrib"
projects[social_media_links][version] = "2.8"

projects[twig_tweak][subdir] = "contrib"
projects[twig_tweak][version] = "2.9"

projects[search_api][subdir] = "contrib"
projects[search_api][version] = "1.20"

projects[simple_sitemap][subdir] = "contrib"
projects[simple_sitemap][version] = "3.10"

projects[views_infinite_scroll][subdir] = "contrib"
projects[views_infinite_scroll][version] = "1.8"

projects[entity_browser][subdir] = "contrib"
projects[entity_browser][version] = "2.6"

projects[media_entity_browser][subdir] = "contrib"
projects[media_entity_browser][version] = "2.0-alpha3"

projects[google_map_field][subdir] = "contrib"
projects[google_map_field][version] = "1.14"

projects[inline_entity_form][subdir] = "contrib"
projects[inline_entity_form][version] = "1.0-rc9"

projects[webform][subdir] = "contrib"
projects[webform][version] = "5.27"

projects[metatag][subdir] = "contrib"
projects[metatag][version] = "1.16"

; Libraries

libraries[slick][directory_name] = "slick"
libraries[slick][type] = "library"
libraries[slick][destination] = "libraries"
libraries[slick][download][type] = "git"
libraries[slick][download][url] = "https://github.com/kenwheeler/slick.git"
libraries[slick][download][tag] = "v1.8.1"

libraries[blazy][directory_name] = "blazy"
libraries[blazy][type] = "library"
libraries[blazy][destination] = "libraries"
libraries[blazy][download][type] = "git"
libraries[blazy][download][url] = "https://github.com/dinbror/blazy.git"
libraries[blazy][download][tag] = "1.8.1"
