<?php

namespace Drupal\connect_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Empty Ads' Block.
 *
 * @Block(
 *   id = "Empty Ads",
 *   admin_label = @Translation("Empty Ads"),
 *   category = @Translation("Connect Theme"),
 * )
 */
class ConnectADBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => '<div class="empty-ad"></div>',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
