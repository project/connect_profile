<?php

namespace Drupal\connect_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Provides a 'Search lens' Block.
 *
 * @Block(
 *   id = "Search Lens",
 *   admin_label = @Translation("Search Lens"),
 *   category = @Translation("Connect Theme"),
 * )
 */
class ConnectSearchLensBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $url = Url::fromRoute('view.search.main_view');
    $link_options = [
      'attributes' => [
        'class' => [
          'search-lens',
        ],
      ],
    ];
    $url->setOptions($link_options);
    $link = Link::fromTextAndUrl('', $url);
    return [
      '#markup' => $link->toString(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
