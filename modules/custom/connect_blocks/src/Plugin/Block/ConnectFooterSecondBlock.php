<?php

namespace Drupal\connect_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Footer Logo' Block.
 *
 * @Block(
 *   id = "Footer Second",
 *   admin_label = @Translation("Footer Second"),
 *   category = @Translation("Connect Theme"),
 * )
 */
class ConnectFooterSecondBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $ownership = t('@ 2018 Connected Ltd. Trademarks and brand are property of their respective owners');
    return [
      '#markup' => '<div class="block-footer-second"><p>' . $ownership . '</p></div>',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
