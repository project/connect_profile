<?php

namespace Drupal\connect_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\media\Entity\Media;
use Drupal\Core\Url;

/**
 * Provides a 'Footer Logo' Block.
 *
 * @Block(
 *   id = "Footer Logo",
 *   admin_label = @Translation("Footer Logo"),
 *   category = @Translation("Connect Theme"),
 * )
 */
class ConnectFooterLogoBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $image_id = theme_get_setting('footer_logo_image', 'connect_theme');
    $media = Media::load($image_id);
    if ($img_entity = $media->get('field_media_image')->first()) {
      if ($file_entity = $img_entity->get('entity')->getTarget()) {
        $url = $file_entity->get('uri')->first()->getString();
      }
    }
    if (isset($url)) {
      $image = [
        '#theme' => 'image',
        '#alt' => 'Logo',
        '#title' => 'Logo',
        '#uri' => $url,
      ];

      return [
        'link' => [
          '#type' => 'link',
          '#title' => $image,
          '#url' => Url::fromRoute('<front>'),
          '#attributes' => [
            'rel' => 'Home',
            'title' => 'Home',
          ],
        ],
      ];
    }
    else {
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
