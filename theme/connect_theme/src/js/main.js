import nav from './modules/navigation';
import sticky from './modules/sticky-header';
import breakingNews from './modules/breaking-news';
import socialLinks from './modules/social-links-icons';
import search from './modules/search-icon';
import ajaxThrobber from './modules/ajax-throbber-placing'

nav.init();
sticky.init();
breakingNews.init();
socialLinks.init();

(function ($) {
  'use strict';

  Drupal.behaviors.main = {
    attach: function(context, settings) {
      ajaxThrobber.init();
      search.init();
    }
  };
})(jQuery);