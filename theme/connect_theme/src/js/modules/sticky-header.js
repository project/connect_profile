export default (function ($) {
  const topHeader = $('#top');
  const header = $('#header');

  function init() {   
    setUpListeners();
  }
  
  function setUpListeners() {
    var	posHederTop = topHeader.offset().top;
    var	posHeder = header.offset().top;
    var	heightHederTop = topHeader.height();
    var	heightHeder = header.height();
    var	heightAll = heightHederTop + heightHeder;

    $(window).scroll(function(){
      if ($(window).scrollTop() > posHederTop) {
        topHeader.addClass('fixed-header');
        $('#page').animate({paddingTop: "" + heightHederTop},0);
      }
      else {
        topHeader.removeClass('fixed-header');
        $('#page').animate({paddingTop: '0'},0);
      }
    });
  }

  return {
    init: init
  }
})(jQuery);