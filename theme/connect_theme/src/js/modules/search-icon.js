export default (function ($) {
  const allTags = '.search-view header .views-row span';
  function init() {
    setUpListeners();
    setupTags();
    isSearchEmpty();
  }

  function setupTags() {
    var currentTags = get_query_variable('tags');
    $.each($(allTags), function () {
      $(this).on('click', function (event) {
        if (this.classList.contains('is-active')) {
          var searchValue = $(this)[0].childNodes[0].data.toString().toLowerCase();
          searchValue = specialCharacterReturner(searchValue).replace('+', ' ');
          for (var i = 0; i < currentTags.length; i++) {
            if(specialCharacterReturner(currentTags[i].toString().toLowerCase()).replace('+', ' ') == searchValue) {
              currentTags.splice(i, 1);
            }
          }
        }
        else {
          currentTags.push($(this)[0].childNodes[0].data.toString().toLowerCase());
        }
        var value = '';
        for (var i in currentTags) {
          value += currentTags[i] != '' ? currentTags[i] + ',' : '';
        }
        value = value.substring(0, value.length - 1);
        $('.search-view form .form-item-tags input').attr('value', value);
        $('.search-view form .form-actions input').click();
      });
    });
  }

  function setUpListeners() {
    var currentTags = get_query_variable('tags');
    $.each($(allTags), function () {
      for (var i in currentTags) {
        const tag = specialCharacterReturner(currentTags[i].toString().toLowerCase()).replace('+', ' ');
        if ($(this)[0].childNodes[0].data.toString().toLowerCase() == tag) {
          this.classList.add('is-active');
          break;
        }
      }
    });
  }

  function specialCharacterReturner(query) {
    query = query.replace(/=/g, '');
    query = query.replace(/%3F/g,'?');
    query = query.replace(/%2C/g,'?');
    query = query.replace(/,/g,'?');
    query = query.replace(/%2B/g, '+');
    return query;
  }

  function get_query_variable(variable) {
    var query = $('form .form-item-' + variable + ' input');
    const attr = query.attr('value');
    var vars = specialCharacterReturner(attr).split('?');
    return vars;
  }

  function isSearchEmpty() {
    if ($(".form-item-search-api-fulltext input").val() === "") {
      $(".form-actions input").attr("disabled", true);
    }

    $(".form-item-search-api-fulltext input").on('change', function () {
      if ($(".form-item-search-api-fulltext input").val() === "") {
        $(".form-actions input").attr("disabled", true);
      }
      else $(".form-actions input").attr("disabled", false);
    })
  }

  return {
    init: init
  };
})(jQuery);

