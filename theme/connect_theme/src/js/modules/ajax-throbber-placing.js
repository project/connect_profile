export default (function ($) {

  function init() {
    setAjaxThrobberPlace();
  }

  function setAjaxThrobberPlace() {
    var beforeSend = Drupal.Ajax.prototype.beforeSend;
    // Add a trigger when beforeSend fires.
    Drupal.Ajax.prototype.beforeSend = function (xmlhttprequest, options) {
      // Only apply our override on specific fields.
      try {
        if (!$('body').hasClass('path-admin')) {
          // Copied and modified from Drupal.ajax.prototype.beforeSend in ajax.js
          $(this.element).prop('disabled', true);
          // Modify the actualy progress throbber HTML.
          this.progress.element = $('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;&nbsp;</div></div>');
          // Change the position of the throbber.
          $(this.element).parent().parent().after(this.progress.element);

        }
        else {
          //Send to the default Drupal Ajax function if we're not looking at our specific field.
          beforeSend.call(this, xmlhttprequest, options);
          $(document).trigger('beforeSend');
        }
      }
      catch (e) {
        beforeSend.call(this, xmlhttprequest, options);
        $(document).trigger('beforeSend');
      }
    };
  }

  return {
    init: init
  };
}) (jQuery);