export default (function ($) {

  function init() {   
    setUpListeners();
  }
  
  function setUpListeners() {
    var carousel = $('#block-views-block-articles-block-1 .view-wrapper');
    $(carousel).wrapAll('<div class="slick-carousel-braking-news"></div>');

    carousel.slick({
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      adaptiveHeight: true,
      autoplay: true,
      autoplaySpeed: 10000,
    });
  }

  return {
    init: init
  }
})(jQuery);