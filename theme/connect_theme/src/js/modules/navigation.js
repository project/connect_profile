export default (function ($) {
  const toggle = $('#block-connect-theme-main-menu');
  const $menu = $('#block-connect-theme-main-menu .menu');
  const $menuLayout = $('.menu--simple-mega-menu-layout');

  const duration = 300;

  function init() {   
    setUpListeners();
    addMenuHeader();
  }
  
  function setUpListeners() {
    toggle.on('click', (e) => {
      const $this = $(e.currentTarget);
      if ($this.hasClass('is-open')) {
        closeMenu();
      } else {
        openMenu();
      }
    });

    $('.hamburger-switch').on('click', (e) => {
      closeMenu();
    });
  }

  function addMenuHeader() {
    $($menu.children(0)[0]).addClass('first-child');
    $menu.prepend('<div class="menu-header"><p>MENU</p><span class="menu-header-nav-button"></span><span class="menu-header-close-button"><p></p></span></div>');
  }

  function openMenu() {
    $('body').addClass('open-menu');
    toggle.addClass('is-open');
  }

  function closeMenu() {
    $('body').removeClass('open-menu');
    toggle.removeClass('is-open');
  }

  return {
    init: init
  }
})(jQuery);