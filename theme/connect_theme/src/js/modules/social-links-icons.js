export default (function ($) {
  const twitter = '<span class="fa-stack fa-lg"><i class="fa fa-square square-twitter fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x" ></i></span>';
  const facebook = '<span class="fa-stack fa-lg"><i class="fa fa-square square-facebook fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x"></i></span>';
  const linkedin = '<span class="fa-stack fa-lg"><i class="fa fa-square square-linkedin fa-stack-2x"></i><i class="fa fa-linkedin fa-stack-1x"></i></span>';
  const youtube = '<span class="fa-stack fa-lg"><i class="fa fa-square square-youtube fa-stack-2x"></i><i class="fa fa-youtube-play fa-stack-1x"></i></span>';

  function init() {
    $('.site-footer__bottom span').each(function (id, element) {
      if (element.classList.contains('fa-twitter')) {
        $(element).replaceWith(twitter);
      }
      if (element.classList.contains('fa-facebook')) {
        $(element).replaceWith(facebook);
      }
      if (element.classList.contains('fa-linkedin')) {
        $(element).replaceWith(linkedin);
      }
      if (element.classList.contains('fa-youtube')) {
        $(element).replaceWith(youtube);
      }
    });
    $('.fa-youtube').replaceWith('<span class="fa fa-youtube-play fa-lg"></span>')
  }

  return {
    init: init
  }

})(jQuery);