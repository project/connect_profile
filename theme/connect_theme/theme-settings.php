<?php

use Drupal\Core\Form\FormStateInterface;
use Drupal\media\Entity\Media;

function connect_theme_form_system_theme_settings_alter(&$form, FormStateInterface &$form_state,
                                                        $form_id = NULL) {

  // Work-around for a core bug affecting admin themes. See issue #943212.
  if (isset($form_id)) {
    return;
  }
  $media = Media::load(theme_get_setting('footer_logo_image'));
  
  $form['footer_logo'] = [
    '#type' => 'details',
    '#title' => t('Footer Logo image'),
    '#open' => TRUE,
  ];

  $form['footer_logo']['settings']['footer_logo_path'] = [
    '#type' => 'textfield',
    '#title' => t('Path to custom logo'),
    '#description' => t('The logo must be in the profile folder.'),
    '#default_value' => !empty(theme_get_setting('footer_logo_path')) ? theme_get_setting('footer_logo_path')
      : drupal_get_path('theme', 'connect_theme').  '/footer_logo.svg',
  ];
  
  $form['footer_logo']['settings']['footer_logo_image'] = [
    '#type' => 'entity_autocomplete',
    '#target_type' => 'media',
    '#title' => 'Footer image',
    '#default_value' => isset($media) ? $media : Media::load(0),
  ];

}