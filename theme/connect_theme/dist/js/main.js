webpackJsonp([0],[
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__fonts_main_css__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__fonts_main_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__fonts_main_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__scss_main_scss__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__scss_main_scss___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__scss_main_scss__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__js_main__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__js_main___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__js_main__);
/* Vendor */

/* Fonts */


/* Styles */


/* Scripts */


/***/ }),
/* 1 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 2 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var _navigation = __webpack_require__(4);var _navigation2 = _interopRequireDefault(_navigation);
var _stickyHeader = __webpack_require__(5);var _stickyHeader2 = _interopRequireDefault(_stickyHeader);
var _breakingNews = __webpack_require__(6);var _breakingNews2 = _interopRequireDefault(_breakingNews);
var _socialLinksIcons = __webpack_require__(7);var _socialLinksIcons2 = _interopRequireDefault(_socialLinksIcons);
var _searchIcon = __webpack_require__(8);var _searchIcon2 = _interopRequireDefault(_searchIcon);
var _ajaxThrobberPlacing = __webpack_require__(9);var _ajaxThrobberPlacing2 = _interopRequireDefault(_ajaxThrobberPlacing);function _interopRequireDefault(obj) {return obj && obj.__esModule ? obj : { default: obj };}

_navigation2.default.init();
_stickyHeader2.default.init();
_breakingNews2.default.init();
_socialLinksIcons2.default.init();

(function ($) {
  'use strict';

  Drupal.behaviors.main = {
    attach: function attach(context, settings) {
      _ajaxThrobberPlacing2.default.init();
      _searchIcon2.default.init();
    } };

})(jQuery);

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = function ($) {
  var toggle = $('#block-connect-theme-main-menu');
  var $menu = $('#block-connect-theme-main-menu .menu');
  var $menuLayout = $('.menu--simple-mega-menu-layout');

  var duration = 300;

  function init() {
    setUpListeners();
    addMenuHeader();
  }

  function setUpListeners() {
    toggle.on('click', function (e) {
      var $this = $(e.currentTarget);
      if ($this.hasClass('is-open')) {
        closeMenu();
      } else {
        openMenu();
      }
    });

    $('.hamburger-switch').on('click', function (e) {
      closeMenu();
    });
  }

  function addMenuHeader() {
    $($menu.children(0)[0]).addClass('first-child');
    $menu.prepend('<div class="menu-header"><p>MENU</p><span class="menu-header-nav-button"></span><span class="menu-header-close-button"><p></p></span></div>');
  }

  function openMenu() {
    $('body').addClass('open-menu');
    toggle.addClass('is-open');
  }

  function closeMenu() {
    $('body').removeClass('open-menu');
    toggle.removeClass('is-open');
  }

  return {
    init: init };

}(jQuery);

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = function ($) {
  var topHeader = $('#top');
  var header = $('#header');

  function init() {
    setUpListeners();
  }

  function setUpListeners() {
    var posHederTop = topHeader.offset().top;
    var posHeder = header.offset().top;
    var heightHederTop = topHeader.height();
    var heightHeder = header.height();
    var heightAll = heightHederTop + heightHeder;

    $(window).scroll(function () {
      if ($(window).scrollTop() > posHederTop) {
        topHeader.addClass('fixed-header');
        $('#page').animate({ paddingTop: "" + heightHederTop }, 0);
      } else
      {
        topHeader.removeClass('fixed-header');
        $('#page').animate({ paddingTop: '0' }, 0);
      }
    });
  }

  return {
    init: init };

}(jQuery);

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = function ($) {

  function init() {
    setUpListeners();
  }

  function setUpListeners() {
    var carousel = $('#block-views-block-articles-block-1 .view-wrapper');
    $(carousel).wrapAll('<div class="slick-carousel-braking-news"></div>');

    carousel.slick({
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      adaptiveHeight: true,
      autoplay: true,
      autoplaySpeed: 10000 });

  }

  return {
    init: init };

}(jQuery);

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = function ($) {
  var twitter = '<span class="fa-stack fa-lg"><i class="fa fa-square square-twitter fa-stack-2x"></i><i class="fa fa-twitter fa-stack-1x" ></i></span>';
  var facebook = '<span class="fa-stack fa-lg"><i class="fa fa-square square-facebook fa-stack-2x"></i><i class="fa fa-facebook fa-stack-1x"></i></span>';
  var linkedin = '<span class="fa-stack fa-lg"><i class="fa fa-square square-linkedin fa-stack-2x"></i><i class="fa fa-linkedin fa-stack-1x"></i></span>';
  var youtube = '<span class="fa-stack fa-lg"><i class="fa fa-square square-youtube fa-stack-2x"></i><i class="fa fa-youtube-play fa-stack-1x"></i></span>';

  function init() {
    $('.site-footer__bottom span').each(function (id, element) {
      if (element.classList.contains('fa-twitter')) {
        $(element).replaceWith(twitter);
      }
      if (element.classList.contains('fa-facebook')) {
        $(element).replaceWith(facebook);
      }
      if (element.classList.contains('fa-linkedin')) {
        $(element).replaceWith(linkedin);
      }
      if (element.classList.contains('fa-youtube')) {
        $(element).replaceWith(youtube);
      }
    });
    $('.fa-youtube').replaceWith('<span class="fa fa-youtube-play fa-lg"></span>');
  }

  return {
    init: init };


}(jQuery);

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = function ($) {
  var allTags = '.search-view header .views-row span';
  function init() {
    setUpListeners();
    setupTags();
    isSearchEmpty();
  }

  function setupTags() {
    var currentTags = get_query_variable('tags');
    $.each($(allTags), function () {
      $(this).on('click', function (event) {
        if (this.classList.contains('is-active')) {
          var searchValue = $(this)[0].childNodes[0].data.toString().toLowerCase();
          searchValue = specialCharacterReturner(searchValue).replace('+', ' ');
          for (var i = 0; i < currentTags.length; i++) {
            if (specialCharacterReturner(currentTags[i].toString().toLowerCase()).replace('+', ' ') == searchValue) {
              currentTags.splice(i, 1);
            }
          }
        } else
        {
          currentTags.push($(this)[0].childNodes[0].data.toString().toLowerCase());
        }
        var value = '';
        for (var i in currentTags) {
          value += currentTags[i] != '' ? currentTags[i] + ',' : '';
        }
        value = value.substring(0, value.length - 1);
        $('.search-view form .form-item-tags input').attr('value', value);
        $('.search-view form .form-actions input').click();
      });
    });
  }

  function setUpListeners() {
    var currentTags = get_query_variable('tags');
    $.each($(allTags), function () {
      for (var i in currentTags) {
        var tag = specialCharacterReturner(currentTags[i].toString().toLowerCase()).replace('+', ' ');
        if ($(this)[0].childNodes[0].data.toString().toLowerCase() == tag) {
          this.classList.add('is-active');
          break;
        }
      }
    });
  }

  function specialCharacterReturner(query) {
    query = query.replace(/=/g, '');
    query = query.replace(/%3F/g, '?');
    query = query.replace(/%2C/g, '?');
    query = query.replace(/,/g, '?');
    query = query.replace(/%2B/g, '+');
    return query;
  }

  function get_query_variable(variable) {
    var query = $('form .form-item-' + variable + ' input');
    var attr = query.attr('value');
    var vars = specialCharacterReturner(attr).split('?');
    return vars;
  }

  function isSearchEmpty() {
    if ($(".form-item-search-api-fulltext input").val() === "") {
      $(".form-actions input").attr("disabled", true);
    }

    $(".form-item-search-api-fulltext input").on('change', function () {
      if ($(".form-item-search-api-fulltext input").val() === "") {
        $(".form-actions input").attr("disabled", true);
      } else
      $(".form-actions input").attr("disabled", false);
    });
  }

  return {
    init: init };

}(jQuery);

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });exports.default = function ($) {

  function init() {
    setAjaxThrobberPlace();
  }

  function setAjaxThrobberPlace() {
    var beforeSend = Drupal.Ajax.prototype.beforeSend;
    // Add a trigger when beforeSend fires.
    Drupal.Ajax.prototype.beforeSend = function (xmlhttprequest, options) {
      // Only apply our override on specific fields.
      try {
        if (!$('body').hasClass('path-admin')) {
          // Copied and modified from Drupal.ajax.prototype.beforeSend in ajax.js
          $(this.element).prop('disabled', true);
          // Modify the actualy progress throbber HTML.
          this.progress.element = $('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;&nbsp;</div></div>');
          // Change the position of the throbber.
          $(this.element).parent().parent().after(this.progress.element);

        } else
        {
          //Send to the default Drupal Ajax function if we're not looking at our specific field.
          beforeSend.call(this, xmlhttprequest, options);
          $(document).trigger('beforeSend');
        }
      }
      catch (e) {
        beforeSend.call(this, xmlhttprequest, options);
        $(document).trigger('beforeSend');
      }
    };
  }

  return {
    init: init };

}(jQuery);

/***/ })
],[0]);
//# sourceMappingURL=main.js.map